from flask import Flask
from src.gob.ec.sercop.lecturaDocumentos.controller.AuthenticationController import init as init_auth_routes
from src.gob.ec.sercop.lecturaDocumentos.controller.LecturaArchivoController import init as init_lectura_archivos
from flask_cors import CORS

def create_app():
    app = Flask(__name__)
    CORS(app, support_credentials=True)
    init_auth_routes(app)
    init_lectura_archivos(app)
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5019, debug=True)
