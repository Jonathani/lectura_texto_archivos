from src.gob.ec.sercop.lecturaDocumentos.utils.Utilidades import Utilidades
from src.gob.ec.sercop.lecturaDocumentos.servicios.LecturaArchivoService import LecturaArchivoService
from PIL import Image
import fitz

if __name__ == '__main__':
    # Imagenes y Texto
    url = "http://localhost:3005/ManualTecnico.pdf"

    #palabra = "mascaras laringuea #2"
    #palabra = "entidad contratante"
    palabra = "WILLIAN"
    #extraer_info_archivo(url, palabra)
    print(LecturaArchivoService.extraer_info_archivo_docx(url, palabra))
    print(Utilidades.descargar_documento(url))
