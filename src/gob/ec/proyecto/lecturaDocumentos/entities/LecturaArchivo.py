from Conexion import Document, StringField, archivo_configuracion

class LecturaArchivo(Document):
    codigo_proceso = StringField()
    descripcion_archivo = StringField()
    id_soli_compra = StringField()
    id_usuario = StringField()
    nombre_archivo = StringField()
    nombre_usuario = StringField()
    palabra_original = StringField()
    texto_archivo = StringField()
    texto_total = StringField()
    fecha_registro = StringField()
    numero_paginas = StringField()
    url_pagina = StringField()
    tipo_archivo = StringField()
    descripcion_compra = StringField()
    codigo_tipo_proceso = StringField()
    tipo_proceso = StringField()
    tipo_de_regimen = StringField()
    fecha_publicacion = StringField()
    valor_adjudicado = StringField()
    fecha_adjudicacion = StringField()
    proveedor = StringField()
    cedula_proveedor = StringField()
    cudim = StringField()
    id_registro_consulta = StringField()

    def to_dict(self):
        return {
            'codigo_proceso': self.codigo_proceso,
            'descripcion_archivo': self.descripcion_archivo,
            'id_soli_compra': self.id_soli_compra,
            'id_usuario': self.id_usuario,
            'nombre_archivo': self.nombre_archivo,
            'nombre_usuario': self.nombre_usuario,
            'palabra_original': self.palabra_original,
            'texto_archivo': self.texto_archivo,
            'texto_total': self.texto_total,
            'fecha_registro': self.fecha_registro,
            'numero_paginas': self.numero_paginas,
            'url_pagina': self.url_pagina,
            'tipo_archivo': self.tipo_archivo,
            'descripcion_compra': self.descripcion_compra ,
            'codigo_tipo_proceso': self.codigo_tipo_proceso ,
            'tipo_proceso': self.tipo_proceso ,
            'tipo_de_regimen': self.tipo_de_regimen ,
            'fecha_publicacion': self.fecha_publicacion ,
            'valor_adjudicado': self.valor_adjudicado ,
            'fecha_adjudicacion': self.fecha_adjudicacion ,
            'proveedor': self.proveedor ,
            'cedula_proveedor': self.cedula_proveedor ,
            'cudim': self.cudim,
            'id_registro_consulta': self.id_registro_consulta
        }
    
    meta = {'db_alias': archivo_configuracion['mongodb']['bdd_alias_mongodb']}
