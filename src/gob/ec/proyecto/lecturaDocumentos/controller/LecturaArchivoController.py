from flask import jsonify, request
from flask_cors import cross_origin
from src.gob.ec.proyecto.lecturaDocumentos.servicios.AuthGuardService import auth_guard
from src.gob.ec.proyecto.lecturaDocumentos.servicios.LecturaArchivoService import LecturaArchivoService

def init(app):

    @app.route('/api/lectura/archivos', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def lectura_archivos():
        url_archivo = request.json['urlArchivo']
        palabras_busqueda = request.json['palabraBusqueda']
        codigo_proceso = request.json['codigoProceso']
        id_soli_compra = request.json['idSoliCompra']
        nombre_archivo = request.json['nombreArchivo']
        descripcion_archivo = request.json['descripcionArchivo']
        tipo_archivo = request.json['tipoArchivo']
        id_usuario = request.json['idUsuario']
        nombre_usuario = request.json['nombreUsuario']
        descripcion_compra = request.json['descripcionCompra']
        codigo_tipo_proceso = request.json['codigoTipoProceso']
        tipo_proceso = request.json['tipoProceso']
        tipo_de_regimen = request.json['tipoDeRegimen']
        fecha_publicacion = request.json['fechaPublicacion']
        valor_adjudicado = request.json['valorAdjudicado']
        fecha_adjudicacion = request.json['fechaAdjudicacion']
        proveedor = request.json['proveedor']
        cedula_proveedor = request.json['cedulaProveedor']
        cudim = request.json['cudim']
        id_registro_consulta = request.json['idRegistroConsulta']
        datos_consulta = {
            "url_archivo": url_archivo,
            "palabras_busqueda": palabras_busqueda,
            "codigo_proceso": codigo_proceso,
            "id_soli_compra": id_soli_compra,
            "nombre_archivo": nombre_archivo,
            "descripcion_archivo": descripcion_archivo,
            "tipo_archivo": tipo_archivo,
            "id_usuario": id_usuario,
            "nombre_usuario": nombre_usuario,
            "descripcion_compra": descripcion_compra,
            "codigo_tipo_proceso": codigo_tipo_proceso,
            "tipo_proceso" : tipo_proceso,
            "tipo_de_regimen" : tipo_de_regimen,
            "fecha_publicacion": fecha_publicacion,
            "valor_adjudicado": valor_adjudicado,
            "fecha_adjudicacion": fecha_adjudicacion,
            "proveedor": proveedor,
            "cedula_proveedor": cedula_proveedor,
            "cudim": cudim,
            "id_registro_consulta": id_registro_consulta,
        }
        try:
            if (url_archivo and palabras_busqueda):
                consulta = LecturaArchivoService.lectura_info_archivos(datos_consulta)
                estado = True
                respuesta = {"status": estado, "consulta": consulta}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response

