from flask import request, jsonify
from src.gob.ec.proyecto.lecturaDocumentos.servicios.AuthProviderService import authenticate
from src.gob.ec.proyecto.lecturaDocumentos.servicios.JwtHandlerService import generate_jwt


def init(app):
    @app.route('/api/authentication', methods=['POST'])
    def auth():
        user = request.json.get('user')
        password = request.json.get('password')
        if not user or not password:
            return jsonify({"message": "User or password missing", "status": 400}), 400

        user_data = authenticate(user, password)
        if not user_data:
            return jsonify({"message": "Invalid credentials", "status": 400}), 400

        token = generate_jwt(payload=user_data, lifetime=60)  # <--- generates a JWT with valid within 1 hour by now
        return jsonify({"token": token.decode(), "status": 200}), 200
