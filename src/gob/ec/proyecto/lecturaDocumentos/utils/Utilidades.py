import pytesseract
import requests
import fitz
import urllib.request
import io
import docx
import os
import shutil
import subprocess
from urllib.parse import quote
from PIL import Image
from hermetrics.levenshtein import Levenshtein
from datetime import datetime
from docx2pdf import convert

class Utilidades:

    @staticmethod
    def lectura_estatica_imagenes(ruta_imagen):
        img = Image.open(ruta_imagen)
        img.load()
        text = pytesseract.image_to_string(img)
        return text

    @staticmethod
    def lectura_imagenes(rotated_image, config_img):
        image_text = pytesseract.image_to_string(rotated_image, config=config_img)
        return image_text

    @staticmethod
    def lectura_imagenes_osd(image_pil):
        osd = pytesseract.image_to_osd(image_pil)
        return osd

    @staticmethod
    def extraer_texto_documentos_pdf(url):
        codificacion = quote(url, safe=":/&")
        response = requests.get(codificacion)
        doc = fitz.open(stream=response.content)
        return doc

    @staticmethod
    def extraer_texto_documentos_docx(url):
        codificacion = quote(url, safe=":/&")
        req = urllib.request.Request(codificacion)
        abrir_link = urllib.request.urlopen(req).read()
        archivo = io.BytesIO(abrir_link)
        doc = docx.Document(archivo)
        return doc

    @staticmethod
    def generate_pdf(doc_path, path):
        subprocess.call(['soffice', '--headless','--convert-to','pdf','--outdir',path, doc_path])
        return doc_path

    @staticmethod
    def descargar_documento(url):
        file = requests.get(url, stream=True)
        nombre_archivo = url.split("/")
        nombre_archivo_aux = nombre_archivo[len(nombre_archivo)-1].split("=")
        dump = file.raw
        location = os.path.abspath(f"/tmp/{nombre_archivo_aux[1]}")
        with open(f"/tmp/{nombre_archivo_aux[1]}", 'wb') as location:
            shutil.copyfileobj(dump, location)

        return nombre_archivo_aux[1]

    @staticmethod
    def validar_si_existe_archivo(url):
        nombre_archivo = url.split("/")
        nombre_archivo_aux = nombre_archivo[len(nombre_archivo)-1].split("=")
        check_file = os.path.isfile(f"/tmp/{nombre_archivo_aux[1]}")
        respuesta = {
          "existe_archivo": check_file,
          "nombre_archivo": nombre_archivo_aux[1]
        }
        return respuesta


    @staticmethod
    def estandarizar_texto(texto):
        texto = texto.lower().strip()
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
            ("ñ", "n"),
            ("_", " "),
            ("-", " "),
            ("\n", " "),
            ("\t", " "),
            ("\r", " "),
            ("\r", " "),
            ("(", ""),
            (")", ""),
            ("{", ""),
            ("}", ""),
            ("[", ""),
            ("]", ""),
        )
        for a, b in replacements:
            texto = texto.replace(a, b).replace(a.upper(), b.upper())
        return texto

    @staticmethod
    def limpiar_texto(texto):
        replacements = (
            ("\n", " "),
            ("\t", " "),
            ("\r", " "),
            ("(", ""),
            (")", ""),
            ("{", ""),
            ("}", ""),
            ("[", ""),
            ("]", ""),
            ("'", ""),
        )
        for a, b in replacements:
            texto = texto.replace(a, b)
        return texto

    @staticmethod
    def obtenNGramas(listaPalabras, n):
        ngramas = []
        for i in range(len(listaPalabras)-(n-1)):
            ngramas.append(listaPalabras[i:i+n])
        return ngramas

    @staticmethod
    def obtener_texto_de_imagen(palabra_1, palabra_2, datos_recibidos):
        lev = Levenshtein()
        resultado = None
        todas_las_palabras = palabra_2.split()
        for lista1 in Utilidades.obtenNGramas(todas_las_palabras, len(palabra_1.split(" "))):
            texto_consecutivo = ""
            for lista2 in lista1:
                texto_consecutivo += lista2 + " "
            texto_consecutivo = texto_consecutivo.strip()
            if (lev.similarity(palabra_1,texto_consecutivo) >= 0.80):
                resultado = {}                
                resultado["palabra_original"] = datos_recibidos['palabra_original']
                resultado["numero_paginas"] = datos_recibidos['page_index']
                resultado["urls_paginas"] = datos_recibidos['file']
                resultado["texto_archivo"] = datos_recibidos['texto_original']
                resultado["fecha_registro"] = datetime.today().strftime("%Y-%m-%d %H:%M:%S")

        return resultado

    @staticmethod
    def obtener_texto_de_imagen_sin_encontrar(datos_recibidos):
        resultado = {}                
        resultado["palabra_original"] = datos_recibidos['palabra_original']
        resultado["numero_paginas"] = None
        resultado["urls_paginas"] = datos_recibidos['file']
        resultado["texto_archivo"] = datos_recibidos['texto_original']
        resultado["fecha_registro"] = datetime.today().strftime("%Y-%m-%d %H:%M:%S")

        return resultado
