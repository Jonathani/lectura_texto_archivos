from src.gob.ec.proyecto.lecturaDocumentos.utils.Utilidades import Utilidades
from src.gob.ec.proyecto.lecturaDocumentos.entities.LecturaArchivo import LecturaArchivo
import datetime
from PIL import Image
import fitz



class LecturaArchivoService:

    @staticmethod
    def lectura_info_archivos(datos_consulta):
    
        validar_existe_archivo = Utilidades.validar_si_existe_archivo(datos_consulta['url_archivo'])
        if validar_existe_archivo["existe_archivo"]:
            nombre_archivo = validar_existe_archivo["nombre_archivo"].split(".")
            if nombre_archivo[1] != "pdf":
                # Transformer to pdf
                Utilidades.generate_pdf(f"/tmp/{nombre_archivo[0]}.{nombre_archivo[1]}", f"/tmp/")
        else:
            nombre_archivo = Utilidades.descargar_documento(datos_consulta['url_archivo']).split(".")
            if nombre_archivo[1] != "pdf":
                # Transformer to pdf
                Utilidades.generate_pdf(f"/tmp/{nombre_archivo[0]}.{nombre_archivo[1]}", f"/tmp/")

        file = f"/tmp/{nombre_archivo[0]}.pdf"
        pdf_file = fitz.open(file)
        datos_consulta_pdf = {        
            "url_archivo": datos_consulta['url_archivo'],
            "palabras_busqueda": datos_consulta['palabras_busqueda'],
            "codigo_proceso": datos_consulta['codigo_proceso'],
            "id_soli_compra": datos_consulta['id_soli_compra'],
            "nombre_archivo": datos_consulta['nombre_archivo'],
            "descripcion_archivo": datos_consulta['descripcion_archivo'],
            "tipo_archivo": datos_consulta['tipo_archivo'],
            "id_usuario": datos_consulta['id_usuario'],
            "nombre_usuario": datos_consulta['nombre_usuario'],
            "pdf_file": pdf_file,
            "descripcion_compra": datos_consulta['descripcion_compra'],
            "codigo_tipo_proceso": datos_consulta['codigo_tipo_proceso'],
            "tipo_proceso" : datos_consulta['tipo_proceso'],
            "tipo_de_regimen" : datos_consulta['tipo_de_regimen'],
            "fecha_publicacion": datos_consulta['fecha_publicacion'],
            "valor_adjudicado": datos_consulta['valor_adjudicado'],
            "fecha_adjudicacion": datos_consulta['fecha_adjudicacion'],
            "proveedor": datos_consulta['proveedor'],
            "cedula_proveedor": datos_consulta['cedula_proveedor'],
            "cudim": datos_consulta['cudim'],
            "id_registro_consulta": datos_consulta['id_registro_consulta']
        }
        resultado = LecturaArchivoService.extraer_info_archivo_pdf(datos_consulta_pdf)
        return resultado

    @staticmethod
    def extraer_info_archivo_pdf(datos_consulta_pdf):
        #def extraer_info_archivo_pdf(url, pdf_file, palabra, codigo_proceso):
        resultado_total = []
        texto_total = ""
        config_img = '--psm 3'
        # config_img = r'--oem 3 --psm 3'
        for page_index in range(len(datos_consulta_pdf['pdf_file'])):
            page = datos_consulta_pdf['pdf_file'][page_index]
            image_list = page.get_images()
            texto_total += "------------ Pagina Nro: ------------"
            if image_list:
                content_text = page.get_text("text")
                texto_total += content_text + " \n "
                palabra_original = datos_consulta_pdf['palabras_busqueda']
                datos_enviar = {
                    'texto_original': content_text,
                    'palabra_original': palabra_original,
                    'page_index': page_index + 1,
                    'file': datos_consulta_pdf['url_archivo']
                }
                palabra_1 = Utilidades.estandarizar_texto(datos_consulta_pdf['palabras_busqueda'])
                palabra_2 = Utilidades.estandarizar_texto(content_text)
                resultado = Utilidades.obtener_texto_de_imagen(palabra_1, palabra_2, datos_enviar)
                if (resultado is not None):
                    resultado_total.append(resultado)
                else:
                    resultado_null = Utilidades.obtener_texto_de_imagen_sin_encontrar(datos_enviar)
                    resultado_total.append(resultado_null)

                print(f"[+] Found a total of {len(image_list)} images in page {page_index}")
                for imagen_index in image_list:
                    try:
                        xref = imagen_index[0]
                        pix = fitz.Pixmap(datos_consulta_pdf['pdf_file'], xref)
                        try:
                            image_pil = Image.frombytes("L", [pix.width, pix.height], pix.samples)
                            image_pil.mode
                            osd = Utilidades.lectura_imagenes_osd(image_pil)
                            rotation_angle = 0
                            for line in osd.split("\n"):
                                if "Rotate: " in line:
                                    rotation_angle = int(line.split(": ")[-1])

                            rotated_image = image_pil.rotate(-rotation_angle, expand=True)
                            image_text = Utilidades.lectura_imagenes(rotated_image, config_img)

                        except Exception as e:
                            image_pil = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                            image_pil.save("imagen_extraida.png")
                            image_text = Utilidades.lectura_estatica_imagenes("imagen_extraida.png")

                        texto_original = image_text
                        texto_total += texto_original + " \n "
                        palabra_original = datos_consulta_pdf['palabras_busqueda']
                        datos_enviar = {
                            'texto_original': texto_original,
                            'palabra_original': palabra_original,
                            'page_index': page_index + 1,
                            'file': datos_consulta_pdf['url_archivo']
                        }
                        palabra_1 = Utilidades.estandarizar_texto(datos_consulta_pdf['palabras_busqueda'])
                        palabra_2 = Utilidades.estandarizar_texto(image_text)
                        resultado = Utilidades.obtener_texto_de_imagen(palabra_1, palabra_2, datos_enviar)
                        if (resultado is not None):
                            resultado_total.append(resultado)
                        else:
                            resultado_null = Utilidades.obtener_texto_de_imagen_sin_encontrar(datos_enviar)
                            resultado_total.append(resultado_null)

                    except Exception as e:
                        e

            else:
                print("[!] No images found on page", page_index)
                content_text = page.get_text("text")
                texto_total += content_text + " \n "
                palabra_original = datos_consulta_pdf['palabras_busqueda']
                datos_enviar = {
                    'texto_original': content_text,
                    'palabra_original': palabra_original,
                    'page_index': page_index + 1,
                    'file': datos_consulta_pdf['url_archivo']
                }
                palabra_1 = Utilidades.estandarizar_texto(datos_consulta_pdf['palabras_busqueda'])
                palabra_2 = Utilidades.estandarizar_texto(content_text)
                resultado = Utilidades.obtener_texto_de_imagen(palabra_1, palabra_2, datos_enviar)
                if resultado is not None:
                    resultado_total.append(resultado)
                else:
                    resultado_null = Utilidades.obtener_texto_de_imagen_sin_encontrar(datos_enviar)
                    resultado_total.append(resultado_null)

        paginas = ""
        texto_archivo = []
        for result in resultado_total:
            if result['numero_paginas']:
                paginas += str(result['numero_paginas']) + ","
                texto_archivo.append(
                    {"texto": Utilidades.limpiar_texto(
                        " ------------ Página Nro: " + str(result['numero_paginas']) + " ------------ \n " + result[
                            'texto_archivo'])}
                )

        texto_total_aux = []
        text_total_aux = texto_total.split("------------ Pagina Nro: ------------")
        for text in text_total_aux:
            if text:
                texto_total_aux.append(
                    {"texto": Utilidades.limpiar_texto(text)}
                )

        resultado_final = {}
        resultado_final['palabraOriginal'] = datos_consulta_pdf['palabras_busqueda']
        resultado_final['numeroPaginas'] = paginas[0:(len(paginas) - 1)]
        resultado_final['urlsPaginas'] = datos_consulta_pdf['url_archivo']
        resultado_final['codigoProceso'] = datos_consulta_pdf['codigo_proceso']
        resultado_final['idSoliCompra'] = datos_consulta_pdf['id_soli_compra']
        resultado_final['nombreArchivo'] = datos_consulta_pdf['nombre_archivo']
        resultado_final['descripcionArchivo'] = datos_consulta_pdf['descripcion_archivo']
        resultado_final['tipoArchivo'] = datos_consulta_pdf['tipo_archivo']
        resultado_final['idUsuario'] = datos_consulta_pdf['id_usuario']
        resultado_final['nombreUsuario'] = datos_consulta_pdf['nombre_usuario']
        resultado_final['descripcion_compra'] = datos_consulta_pdf['descripcion_compra']
        resultado_final['codigo_tipo_proceso'] = datos_consulta_pdf['codigo_tipo_proceso']
        resultado_final['tipo_proceso'] = datos_consulta_pdf['tipo_proceso']
        resultado_final['tipo_de_regimen'] = datos_consulta_pdf['tipo_de_regimen']
        resultado_final['fecha_publicacion'] = datos_consulta_pdf['fecha_publicacion']
        resultado_final['valor_adjudicado'] = datos_consulta_pdf['valor_adjudicado']
        resultado_final['fecha_adjudicacion'] = datos_consulta_pdf['fecha_adjudicacion']
        resultado_final['proveedor'] = datos_consulta_pdf['proveedor']
        resultado_final['cedula_proveedor'] = datos_consulta_pdf['cedula_proveedor']
        resultado_final['cudim'] = datos_consulta_pdf['cudim']
        resultado_final['id_registro_consulta'] = datos_consulta_pdf['id_registro_consulta']
        resultado_final['textoArchivo'] = texto_archivo
        resultado_final['textoTotal'] = texto_total_aux
        
        try:
            if (len(paginas[0:(len(paginas) - 1)]) > 0):
                lectura_de_documento = LecturaArchivo()
                lectura_de_documento.codigo_proceso = datos_consulta_pdf['codigo_proceso']
                lectura_de_documento.descripcion_archivo = datos_consulta_pdf['descripcion_archivo']
                lectura_de_documento.id_soli_compra = datos_consulta_pdf['id_soli_compra']
                lectura_de_documento.id_usuario = datos_consulta_pdf['id_usuario']
                lectura_de_documento.nombre_archivo = datos_consulta_pdf['nombre_archivo']
                lectura_de_documento.nombre_usuario = datos_consulta_pdf['nombre_usuario']
                lectura_de_documento.palabra_original = datos_consulta_pdf['palabras_busqueda'].lower()
                lectura_de_documento.texto_archivo = str(texto_archivo)
                lectura_de_documento.texto_total = str(texto_total_aux)
                lectura_de_documento.fecha_registro = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                lectura_de_documento.numero_paginas = paginas[0:(len(paginas) - 1)]
                lectura_de_documento.descripcion_compra = datos_consulta_pdf['descripcion_compra']
                lectura_de_documento.codigo_tipo_proceso = datos_consulta_pdf['codigo_tipo_proceso']
                lectura_de_documento.tipo_proceso = datos_consulta_pdf['tipo_proceso']
                lectura_de_documento.tipo_de_regimen = datos_consulta_pdf['tipo_de_regimen']
                lectura_de_documento.fecha_publicacion = datos_consulta_pdf['fecha_publicacion']
                lectura_de_documento.valor_adjudicado = datos_consulta_pdf['valor_adjudicado']
                lectura_de_documento.fecha_adjudicacion = datos_consulta_pdf['fecha_adjudicacion']
                lectura_de_documento.proveedor = datos_consulta_pdf['proveedor']
                lectura_de_documento.cedula_proveedor = datos_consulta_pdf['cedula_proveedor']
                lectura_de_documento.url_pagina = datos_consulta_pdf['url_archivo']
                lectura_de_documento.tipo_archivo = datos_consulta_pdf['tipo_archivo']
                lectura_de_documento.cudim = datos_consulta_pdf['cudim']
                lectura_de_documento.id_registro_consulta = datos_consulta_pdf['id_registro_consulta']
                lectura_de_documento.save()
        except Exception as e:
            print(f">>>>>>>> {e}" )
        
        return resultado_final

